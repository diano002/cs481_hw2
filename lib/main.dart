import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.

  @override
  Widget build(BuildContext context) {
    //Title of Layout
    Widget titleSection = Container(
      padding: const EdgeInsets.all(20),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            'Chicago Deep Dish Pizza',
            style: TextStyle(
              fontSize: 27,
              fontWeight: FontWeight.bold,
              color: Colors.blue,
            ),
          )
        ],
      ),
    );

    //Save&Get
    Widget saveAndGet = Container(
      padding: const EdgeInsets.only(right: 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Container(
            padding: const EdgeInsets.only(left: 20, right: 30),
            child: Text(
              "Ingredients:",
              style: TextStyle(
                fontSize: 15, color: Colors.deepOrange
              ),

            ),
          ),
          Container(
            padding: const EdgeInsets.only(
              left: 165,
              right: 15,
              top: 5,
            ),
            child: Text(
              'Save',
              style: TextStyle(
                fontSize: 15,
                color: Colors.green,
              ),
            ),
          ),
          Container(
            padding: const EdgeInsets.only(right: 15, left: 5, top: 5),
            child: Text(
              'Get',
              style: TextStyle(
                fontSize: 15,
                color: Colors.blue,
              ),
            ),
          )
        ],
      ),
    );

    //Ingredients Needed
    Widget ingredientsList = Container(
      child: Column(
        children: [
          _buildIngredientRow('1 pound pizza dough'),
          _buildIngredientRow('6 - 10 slices vegan white cheese'),
          _buildIngredientRow('4 links Beyond Meat sausage'),
          _buildIngredientRow('1 (16-ounce) bag frozen spinach'),
          _buildIngredientRow('1 tablespoon pizza seasoning'),
          _buildIngredientRow(
              '1 (28-ounce) can fire-roasted or regular crushed tomatoes'),
          _buildIngredientRow('2 tablespoons nutritional yeast'),
        ],
      ),
    );

    //ImageSection
    Widget imageAndButtons = Container(
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Column(
            //crossAxisAlignment: CrossAxisAlignment.start,
            //mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Container(
                padding: const EdgeInsets.only(right: 10, top: 30),
                child: Image(
                  image: NetworkImage('https://1.bp.blogspot.com/-HI6foVVI7ds/XytHNBdL3XI/AAAAAAAAIX8/N_CJSaQ7_n8PFV-J4XpTm-2eqOvpLI3GwCLcBGAsYHQ/d/pizza_slice.jpg'),
                  width: 250,
                  height: 240,
                ),
              ),
            ], //children for column
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                padding: EdgeInsets.only(right: 20),
                child: RaisedButton(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(18.0),
                    side: BorderSide(color: Colors.lightBlueAccent),


                  ),
                 padding: EdgeInsets.all(8.0),
                  color: Colors.lightBlueAccent,
                  onPressed: () {},
                  child: Text(
                    "+Later",
                    style: TextStyle(fontSize: 20, color: Colors.black),
                  )
                )
              ),
              Container(
                  padding: EdgeInsets.only(top: 20, right: 20),
                  child: RaisedButton(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(18.0),
                        side: BorderSide(color: Colors.lightGreen),
                      ),
                      padding: EdgeInsets.all(8.0),
                      color: Colors.lightGreen,
                      onPressed: () {},
                      child: Text(
                        "Cook Now",
                        style: TextStyle(fontSize: 20, color: Colors.black),
                      )
                  )
              )
            ],
          )
        ], //children for row

        ),

    );

    return MaterialApp(
      title: 'CS481 HW2',
      home: Scaffold(
        appBar: AppBar(
          title: Text('Zsu\'s Vegan Pantry'),
        ),
        body: ListView(
          children: [
            titleSection,
            saveAndGet,
            ingredientsList,
            imageAndButtons
          ],
        ),
      ),
    );
  }
}

Container _buildIngredientRow(String ingredient) {
  return Container(
    padding: const EdgeInsets.only(right: 15),
    child: Row(
      children: [
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                padding: const EdgeInsets.only(left: 20),
                child: Text(
                  ingredient,
                  style: TextStyle(fontSize: 17),
                ),
              )
            ],
          ),
        ),
        _buildCheckbox(false, Colors.green),
        _buildCheckbox(true, Colors.blue)
      ],
    ),
  );
}

Widget _buildCheckbox(bool valueBool, Color color) {
  return new Checkbox(
      value: valueBool, activeColor: color, onChanged: (bool newValue) {});
}
